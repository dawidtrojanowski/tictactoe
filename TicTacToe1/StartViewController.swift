//
//  StartViewController.swift
//  TicTacToe1
//
//  Created by Dawid Trojanowski on 18/12/2018.
//  Copyright © 2018 Dawid Trojanowski. All rights reserved.
//

import UIKit

var urlAdress = ""

class StartViewController: UIViewController {

    @IBOutlet weak var url: UILabel!
    @IBOutlet weak var textView: UITextField!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let roundedButtons = [playButton, changeButton, saveButton, cancelButton]
        
        for button in roundedButtons {
            button?.layer.cornerRadius = 0.5 * (button?.bounds.size.height)!
        }
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    
    @IBAction func playButtonTapped(_ sender: Any) {
        
        if url.text != "" {
            textView.isHidden = true
            saveButton.isHidden = true
            cancelButton.isHidden = true
            changeButton.isEnabled = true
            urlAdress = url.text!
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let resultViewController = storyBoard.instantiateViewController(withIdentifier: "MainPlay") as! GameViewController
            self.present(resultViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func changeButtonTapped(_ sender: Any) {
        textView.isHidden = false
        saveButton.isHidden = false
        cancelButton.isHidden = false
        changeButton.isEnabled = false
        textView.text = url.text!
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        if textView.text != "" {
            url.text = textView.text
            textView.isHidden = true
            saveButton.isHidden = true
            cancelButton.isHidden = true
            changeButton.isEnabled = true
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        
        textView.isHidden = true
        saveButton.isHidden = true
        cancelButton.isHidden = true
        changeButton.isEnabled = true
    }
    
}
