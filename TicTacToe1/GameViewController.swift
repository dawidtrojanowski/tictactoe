//
//  GameViewController.swift
//  TicTacToe1
//
//  Created by Dawid Trojanowski on 16/12/2018.
//  Copyright © 2018 Dawid Trojanowski. All rights reserved.
//

import UIKit
import Alamofire

class GameViewController: UIViewController {

    var levelOfHardness = 1
    
    @IBOutlet weak var name0: UIButton!
    @IBOutlet weak var name1: UIButton!
    @IBOutlet weak var name2: UIButton!
    @IBOutlet weak var name3: UIButton!
    @IBOutlet weak var name4: UIButton!
    @IBOutlet weak var name5: UIButton!
    @IBOutlet weak var name6: UIButton!
    @IBOutlet weak var name7: UIButton!
    @IBOutlet weak var name8: UIButton!
    
    @IBOutlet weak var mediumButton: UIButton!
    @IBOutlet weak var easyButton: UIButton!
    @IBOutlet weak var hardButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var playerMarkButton: UIButton!
    
    
    
    var url = URLs.init()
    var player = "X"
    var opponent = "O"
    var startingMark = "X"
    var gameState = ["","","","","","","","",""]
    let winningCombinations = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]
    var gameIsActive = true
    var boardDto:BoardDto?
    var wins = 0
    var loses = 0
    var draws = 0
    var changeLevel = false
    
    @IBOutlet weak var winsLabel: UILabel!
    @IBOutlet weak var drawsLabel: UILabel!
    @IBOutlet weak var losesLabel: UILabel!
    @IBOutlet weak var playerMarkLabel: UILabel!
    
    var buttons: [UIButton] = [UIButton]()
    var levelButtons = [UIButton]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttons = [name0, name1, name2, name3, name4, name5, name6, name7, name8]
        levelButtons = [easyButton, mediumButton, hardButton]
        
        levelButtons[levelOfHardness].setTitleColor(UIColorFromRGB(rgbValue: 0x434343), for: .normal)
        
        closeButton.layer.cornerRadius = 0.5 * closeButton.bounds.size.height
        
        for i in levelButtons {
            i.layer.cornerRadius = 0.5 * i.bounds.size.height
        }
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(GameViewController.longTap(_:)))
        playerMarkButton.addGestureRecognizer(longGesture)
        
        AF.request(url.createGame!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: [:]).validate().responseString { response in
            if response.result.value != nil {
                    self.url = URLs.init(gameId: (response.result.value)!)
            } else if response.result.error != nil {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        if gameIsActive == true && gameState[sender.tag - 1] == "" {
            
            gameState[sender.tag - 1] = player
            if player == "X" {
                sender.setTitleColor(UIColorFromRGB(rgbValue: 0xee6363), for: .normal)
            } else {
                sender.setTitleColor(UIColorFromRGB(rgbValue: 0x54b8fd), for: .normal)
            }
            
            sender.setTitle(player, for: .normal)
            
            
            let moveDto = MoveDto(mark: player, field: sender.tag - 1)
            
            AF.request(self.url.setFieldByUser!, method: .post, parameters: moveDto.toJSON(), encoding: JSONEncoding.default).validate().response { result in
                
                let aiMove = AiMoveDto(mark: self.opponent, level: self.levelOfHardness)
                AF.request(self.url.setFielByAi!, method: .post, parameters: aiMove.toJSON(), encoding: JSONEncoding.default).validate().response { data in
                    self.getResult ()
                }
            }
        }
    }
    
    func checkIfWin() {
        for combination in winningCombinations
        {
            if gameState[combination[0]] != "" && gameState[combination[0]] == gameState[combination[1]] && gameState[combination[1]] == gameState[combination[2]]
            {
                gameIsActive = false
                
                if gameState[combination[0]] == player {
                    wins += 1
                    winsLabel.text = String(wins)
                    break
                } else {
                    loses += 1
                    losesLabel.text = String(loses)
                    break
                }
            }
        }
        
        if gameIsActive == true {

            var isDraw = true
            gameState.forEach { field in
                if field == "" {
                    isDraw = false
                }
            }

            if isDraw == true {
                gameIsActive = false
                draws += 1
                drawsLabel.text = String(draws)
            }
        }
        
        print(gameIsActive)
        if gameIsActive == false {
            gameIsActive = true
            resetGame()
        }
    }
    
    @IBAction func closeGame(_ sender: Any) {
        AF.request(url.closeGame!, method: .delete).resume()
        dismiss(animated: true, completion: nil)    }
    
    func resetGame() {
        AF.request(url.resetGame!, method: .post).validate().response { response in
            
            self.getResult()
        }
        
        if changeLevel == true {
            let aiMove = AiMoveDto(mark: self.opponent, level: self.levelOfHardness)
            if startingMark == opponent {
                AF.request(self.url.setFielByAi!, method: .post, parameters: aiMove.toJSON(), encoding: JSONEncoding.default).validate().response { data in
                    self.getResult()
                }
            }
        } else {
            if self.startingMark == self.player {
                self.player = self.opponent
                self.opponent = self.startingMark
                let aiMove = AiMoveDto(mark: self.opponent, level: self.levelOfHardness)
                AF.request(self.url.setFielByAi!, method: .post, parameters: aiMove.toJSON(), encoding: JSONEncoding.default).validate().response { data in
                    self.getResult ()
                }
            } else {
                self.opponent = self.player
                self.player = self.startingMark
            }
            
            if player == "X" {
                playerMarkButton.setTitleColor(UIColorFromRGB(rgbValue: 0xee6363), for: .normal)
            } else {
                playerMarkButton.setTitleColor(UIColorFromRGB(rgbValue: 0x54b8fd), for: .normal)
            }
            playerMarkButton.setTitle(player, for: .normal)
        }
        changeLevel = false
        
    }

    func getResult() {
        
        AF.request(url.getResult!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: [:]).validate().responseJSON { data in
            
            if data.result.isSuccess == true {
                
                let board = BoardDto.fromJSON(map: data.result.value! as! [String : Any])
                self.gameState = board
                for i in 0...8 {
                    
                    if self.gameState[i] == "X" {
                        
                        self.buttons[i].setTitleColor(self.UIColorFromRGB(rgbValue: 0xee6363), for: .normal)
                        self.buttons[i].setTitle(self.gameState[i], for: .normal)
                    } else {
                        self.buttons[i].setTitleColor(self.UIColorFromRGB(rgbValue: 0x54b8fd), for: .normal)
                        self.buttons[i].setTitle(self.gameState[i], for: .normal)
                    }
                }
                self.checkIfWin()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    
    }

    @IBAction func gameLevel(_ sender: UIButton) {
        changeLevel = true
        levelButtons[levelOfHardness].setTitleColor(.white, for: .normal)
        levelOfHardness = sender.tag - 1
        sender.setTitleColor(UIColorFromRGB(rgbValue: 0x434343), for: .normal)
        resetGame()
    }
    
    @objc func longTap(_ sender: UIGestureRecognizer) {
        if sender.state == .began {
            resetGame()
        }
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
