//
//  BoardDto.swift
//  TicTacToe1
//
//  Created by Dawid Trojanowski on 19/12/2018.
//  Copyright © 2018 Dawid Trojanowski. All rights reserved.
//

import Foundation

class BoardDto {
    
    var fields = [String]()
    
    func toJSON() -> [String : Any] {
        return ["fields" : fields as [Any]]
    }
    
    static func fromJSON(map: [String : Any] ) -> [String]{
        return map["fields"] as! [String]
    }
}
