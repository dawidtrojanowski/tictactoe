//
//  URLs.swift
//  TicTacToe1
//
//  Created by Dawid Trojanowski on 20/12/2018.
//  Copyright © 2018 Dawid Trojanowski. All rights reserved.
//

import Foundation

struct URLs {
    
    private var mainUrl:String = urlAdress + "api/tictactoe/"
    var createGame:String? = nil
    var closeGame:String? = nil
    var getResult:String? = nil
    var setFieldByUser:String? = nil
    var setFielByAi:String? = nil
    var resetGame:String? = nil
    
    init() {
        createGame = mainUrl + "/createGame"
    }
    
    init(gameId: String) {
        getResult = mainUrl + gameId + "/result"
        setFieldByUser = mainUrl + gameId + "/setFieldByUser"
        setFielByAi = mainUrl + gameId + "/setFieldByAi"
        closeGame = mainUrl + gameId + "/closeGame"
        resetGame = mainUrl + gameId + "/resetGame"
    }
}
