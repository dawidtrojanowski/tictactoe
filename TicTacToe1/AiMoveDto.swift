//
//  AiMoveDto.swift
//  TicTacToe1
//
//  Created by Dawid Trojanowski on 27/12/2018.
//  Copyright © 2018 Dawid Trojanowski. All rights reserved.
//

import Foundation

class AiMoveDto {
    
    public static let MODE_EASY: Int = 0;
    public static let MODE_MEDIUM: Int = 1;
    public static let MODE_HARD: Int = 2;
    
    var mark: String
    var level: Int
    
    init(mark: String, level: Int) {
        self.mark = mark
        self.level = level
    }
    
    func toJSON() -> [String : Any] {
        return [
            "mark" : mark as Any,
            "level" : level as Any]
    }
}
