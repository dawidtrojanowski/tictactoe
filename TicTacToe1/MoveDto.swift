//
//  MoveDto.swift
//  TicTacToe1
//
//  Created by Dawid Trojanowski on 21/12/2018.
//  Copyright © 2018 Dawid Trojanowski. All rights reserved.
//

import Foundation

class MoveDto {
    var mark: String
    var field: Int

    init(mark: String, field: Int) {
        self.mark = mark
        self.field = field
    }
    
    func toJSON() -> [String : Any] {
        return [
            "mark" : mark as Any,
            "field" : field as Any]
    }
}
