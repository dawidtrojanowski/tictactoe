//
//  QRViewController.swift
//  TicTacToe1
//
//  Created by Dawid Trojanowski on 15/12/2018.
//  Copyright © 2018 Dawid Trojanowski. All rights reserved.
//

import UIKit
import FirebaseInstanceID

class QRViewController: UIViewController {

    @IBOutlet weak var CodeQR: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func generateQR(_ sender: Any) {
        InstanceID.instanceID().instanceID { (result, error) in
            
            if let QRresult = result {
                var QRCode:String
                QRCode = (QRresult.token)
                let data = QRCode.data(using: .ascii, allowLossyConversion: false)
                let filter = CIFilter(name: "CIQRCodeGenerator")
                filter?.setValue(data, forKey: "inputMessage")
                
                let img = UIImage(ciImage: (filter?.outputImage)!)
                
                self.CodeQR.image = img
                self.CodeQR.isHidden = false
                
                
                
            }
        }
        
    }
    
    @IBAction func scanQR(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let resultViewController = storyBoard.instantiateViewController(withIdentifier: "scanner") as! ScannerViewController
    
        self.present(resultViewController, animated:true, completion:nil)
        InstanceID.instanceID().instanceID { (result, error) in
            
        }
    }
}
